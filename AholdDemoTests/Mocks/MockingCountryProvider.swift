//
//  MockingCountryProvider.swift
//  AholdDemoTests
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation
@testable import AholdDemo

class MockingCountryProvider : DataProvider<Country> {
	override func loadData(into manager: Manager<Country>) {
	}
}
