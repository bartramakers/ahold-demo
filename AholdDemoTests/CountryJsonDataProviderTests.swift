//
//  CountryJsonDataProviderTests.swift
//  AholdDemoTests
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import XCTest
@testable import AholdDemo

class CountryJsonDataProviderTests : XCTestCase {
	var dataProvider: CountryFromJsonOnDiskDataProvider!
	
	override func setUp() {
		super.setUp()
		dataProvider = CountryFromJsonOnDiskDataProvider(jsonFileName: "Countries", bundle: Bundle.main)
	}
	
	func testDataProviderInit() {
		XCTAssertEqual(dataProvider.resourceName, "Countries", "ResourceName should be taken from init")
		XCTAssertEqual(dataProvider.bundle, Bundle.main, "Bundle should be taken from init")
	}
	
	func testLoadingIntoManager() {
		let countries = Manager<Country>()
		XCTAssertTrue(countries.all.isEmpty, "Manager<Country> should be empty on init")
		
		dataProvider.loadData(into: countries)
		XCTAssertFalse(countries.all.isEmpty, "Manager<Country> should have data after loadData(into:)")
	}
}
