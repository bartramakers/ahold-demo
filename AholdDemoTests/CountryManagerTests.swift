//
//  CountryManagerTests.swift
//  AholdDemoTests
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import XCTest
@testable import AholdDemo

class CountryManagerTests: XCTestCase {
    override func setUp() {
        super.setUp()
		app = ApplicationContext(countryDataProvider: MockingCountryProvider())
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCountryManagerStoreEmpty() {
		XCTAssertTrue(app.countries.all.isEmpty, "Store should be empty")
		app.countries.start()
		XCTAssertTrue(app.countries.all.isEmpty, "Store should be empty")
    }
	
	func testCountryManagerStoreNotEmpty() {
		XCTAssertTrue(app.countries.all.isEmpty, "Store should be empty")
		app.countries.start()
		app.countries.add(Country(code: "AA", name: "Country A"))
		XCTAssertEqual(app.countries.all.count, 1, "Store should hold 1 country")
		
		app.countries.add(Country(code: "BB", name: "Country B"))
		XCTAssertEqual(app.countries.all.count, 2, "Store should hold 2 countries")
		
		app.countries.add(Country(code: "AA", name: "Country A"))
		XCTAssertEqual(app.countries.all.count, 3, "Store should hold 3 countries, ignoring possible duplicates")
	}
    
}
