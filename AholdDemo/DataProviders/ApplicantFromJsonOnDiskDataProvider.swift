//
//  ApplicantFromJsonDataProvider.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

class ApplicantFromJsonOnDiskDataProvider : DataProvider<Applicant>, JsonFromDiskDataLoader {
	typealias ModelToProvide = Applicant
	typealias ModelToLoad = Applicant
	
	var bundle: Bundle
	var resourceName: String
	
	init(jsonFileName: String = "Applicants", bundle: Bundle = Bundle.main) {
		self.bundle = bundle
		self.resourceName = jsonFileName
	}
	
	override func loadData(into manager: Manager<ModelToProvide>) {
		let models = self.loadModelsFromResource()
		manager.add(models)
	}

}
