//
//  CountryFromJsonDataProvider.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

class CountryFromJsonOnDiskDataProvider : DataProvider<Country>, JsonFromDiskDataLoader {
	typealias ModelToProvide = Country
	typealias ModelToLoad = Country

	var bundle: Bundle
	var resourceName: String
	
	init(jsonFileName: String = "Countries", bundle: Bundle = Bundle.main) {
		self.bundle = bundle
		self.resourceName = jsonFileName
	}
	
	override func loadData(into manager: Manager<ModelToProvide>) {
		let models = self.loadModelsFromResource()
		manager.add(models)
	}
}
