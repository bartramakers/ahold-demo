//
//  ApplicantFromWebDataProvider.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

class ApplicantFromWebDataProvider : DataProvider<Applicant>, JsonFromWebDataLoader {
	typealias ModelToProvide = Applicant
	typealias ModelToLoad = Applicant
	
	var url: URL
	
	init(url: URL) {
		self.url = url
	}
	
	override func loadData(into manager: Manager<ModelToProvide>) {
		let models = self.loadModelsFromResource()
		manager.add(models)
	}
	
}
