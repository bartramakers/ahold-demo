//
//  DataProvider.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal class DataProvider<ModelToProvide> : DataProviding {
	typealias Model = ModelToProvide
	func loadData(into manager: Manager<ModelToProvide>) {}
}
