//
//  CountryManager.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal class CountryManager : Manager<Country> {
	func country(withCode code: String) -> Country? {
		return self.all.filter { $0.code.uppercased() == code.uppercased() }.first
	}
}
