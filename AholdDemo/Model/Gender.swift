//
//  Gender.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal enum Gender: String, Codable {
	case male = "M", female = "F"
}
