//
//  Applicant.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import UIKit

internal class Applicant : ApplicantModel {
	var firstName: String?
	var lastName: String?
	var gender: Gender?
	
	var currentPlace: City?
	var emailAddress: String?
	var phoneNumber: String?
	
	var dateOfBirth: Date?
	var placeOfBirth: City?
	
	var photo: UIImage?

	init(firstName: String, lastName: String, place: City, gender: Gender, email: String, phone: String, dateOfBirth: Date, placeOfBirth: City) {
		self.firstName = firstName
		self.lastName = lastName
		self.gender = gender
		self.currentPlace = place
		self.emailAddress = email
		self.phoneNumber = phone
		self.dateOfBirth = dateOfBirth
		self.placeOfBirth = placeOfBirth
	}
	
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		firstName = try values.decode(String.self, forKey: .firstName)
		lastName = try values.decode(String.self, forKey: .lastName)
		gender = try values.decode(Gender.self, forKey: .gender)
		
		currentPlace = try values.decode(City.self, forKey: .currentPlace)
		emailAddress = try values.decode(String.self, forKey: .emailAddress)
		phoneNumber = try values.decode(String.self, forKey: .phoneNumber)
		
		dateOfBirth = try values.decode(Date.self, forKey: .dateOfBirth)
		placeOfBirth = try values.decode(City.self, forKey: .placeOfBirth)

		if 	let photoDataString = try? values.decode(String.self, forKey: .photoData),
			let photoData = Data(base64Encoded: photoDataString, options: .ignoreUnknownCharacters)
		{
			photo = UIImage(data: photoData)
		}
	}
}

fileprivate extension Applicant {
	enum CodingKeys: String, CodingKey {
		case firstName, lastName, gender
		case currentPlace, emailAddress, phoneNumber
		case dateOfBirth, placeOfBirth
		case photoData
	}
}
