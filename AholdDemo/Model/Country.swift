//
//  Country.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal struct Country : Decodable {
	var code: String
	var name: String
	
	init(code: String, name: String) {
		self.code = code
		self.name = name
	}
}
