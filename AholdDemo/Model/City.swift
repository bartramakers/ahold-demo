//
//  City.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal class City: Decodable {
	var name: String
	var country: Country?
	
	init(name: String, country: Country) {
		self.name = name
		self.country = country
	}
	
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decode(String.self, forKey: .city)
		
		if let countryCode = try? values.decode(String.self, forKey: .countryCode) {
			country = app.countries.country(withCode: countryCode)
		}
	}
}

fileprivate extension City {
	enum CodingKeys: String, CodingKey {
		case city
		case countryCode
	}
	
	enum CountryCodingKeys: String, CodingKey {
		case countryCode
	}
}
