//
//  ApplicantViewController.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import UIKit

class ApplicantViewController: UIViewController {
	// MARK: IBOutlets
	@IBOutlet weak var photoImageView: UIImageView!
	@IBOutlet weak var nameValueLabel: UILabel!
	@IBOutlet weak var genderValueLabel: UILabel!
	@IBOutlet weak var birthdayValueLabel: UILabel!
	@IBOutlet weak var birthPlaceValueLabel: UILabel!
	@IBOutlet weak var currentLocationValueLabel: UILabel!
	@IBOutlet weak var emailValueLabel: UILabel!
	@IBOutlet weak var phoneValueLabel: UILabel!
	
	// MARK: Reusables
	static let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		formatter.timeStyle = .none
		return formatter
	}()
}

extension ApplicantViewController {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Note: In a normal situation, this ViewController would be filled with an ApplicantViewModel by
		// an external source. For example, after selecting an applicant in a list. In this case, we do a load
		// from the ApplicantManager directly
		
		if let applicant = app.applicants.firstApplicant {
			self.apply(viewModel: ApplicantViewModel(model: applicant))
		}
	}
}

// MARK: - Filling from ViewModel

extension ApplicantViewController {
	func apply(viewModel: ApplicantViewModel) {
		fillName(from: viewModel.model)
		fillPhoto(from: viewModel.model)
		fillGender(from: viewModel.model)
		fillBirth(from: viewModel.model)
		fillContactInfo(from: viewModel.model)
	}
}

// MARK: Filling from Providing models

extension ApplicantViewController {
	func fillName(from model: ProvidingName) {
		navigationItem.title = model.fullName
		nameValueLabel.text = model.fullName
	}
	
	func fillPhoto(from model: ProvidingPhoto) {
		photoImageView.image = model.photo
	}
	
	func fillGender(from model: ProvidingGender) {
		genderValueLabel.text = model.gender?.displayValue.capitalized ?? "-"
	}
	
	func fillBirth(from model: ProvidingBirthDetails) {
		birthPlaceValueLabel.text = model.placeOfBirth?.displayValue ?? "-"
		birthdayValueLabel.text = model.dateOfBirth?.displayValue ?? "-"
	}
	
	func fillContactInfo(from model: ProvidingContactInfo) {
		currentLocationValueLabel.text = model.currentPlace?.displayValue ?? "-"
		emailValueLabel.text = model.emailAddress ?? "-"
		phoneValueLabel.text = model.phoneNumber ?? "-"
	}
}

// MARK: - Fileprivate display helpers

fileprivate extension Gender {
	var displayValue: String {
		switch self {
		case .male: return "male"
		case .female: return "female"
		}
	}
}

fileprivate extension City {
	var displayValue: String {
		var displayValue = self.name
		if let country = self.country {
			displayValue += ", " + country.name
		}
		return displayValue
	}
}

fileprivate extension Date {
	var displayValue: String {
		return ApplicantViewController.dateFormatter.string(from: self)
	}
}
