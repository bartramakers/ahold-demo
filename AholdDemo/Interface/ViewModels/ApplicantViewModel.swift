//
//  ApplicantViewModel.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

struct ApplicantViewModel {
	typealias Model = ProvidingName & ProvidingGender & ProvidingContactInfo & ProvidingBirthDetails & ProvidingPhoto
	
	let model: Model
	
	init(model: Model) {
		self.model = model
	}
}
