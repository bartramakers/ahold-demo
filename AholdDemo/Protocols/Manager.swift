//
//  Manager.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal class Manager<Model> {
	private var dataProvider: DataProvider<Model>
	private var store: [Model] = []
	
	init(dataProvider: DataProvider<Model> = DataProvider<Model>()) {
		self.dataProvider = dataProvider
	}
	
	func start() {
		self.dataProvider.loadData(into: self)
	}
}

// MARK - Calculated properties
extension Manager {
	var all: [Model] {
		return self.store
	}
}

// MARK - Synchronized store modification
extension Manager {
	func add(_ modelInstances: [Model]) {
		objc_sync_enter(store)
		defer { objc_sync_exit(store) }
		
		store.append(contentsOf: modelInstances)
	}
	
	func add(_ modelInstance: Model) {
		objc_sync_enter(store)
		defer { objc_sync_exit(store) }
		
		store.append(modelInstance)
	}
}
