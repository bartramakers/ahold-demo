//
//  DataSource.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 02/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

internal protocol DataSource {
	associatedtype Model
	
	func loadData()
}
