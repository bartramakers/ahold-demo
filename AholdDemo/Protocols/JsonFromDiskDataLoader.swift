//
//  JsonDataLoader.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

protocol JsonFromDiskDataLoader: JsonDataLoader {
	var bundle: Bundle { get }
	var resourceName: String { get }
}

extension JsonFromDiskDataLoader {
	var resourceData: Data? {
		guard let fileURL = self.bundle.url(forResource: resourceName, withExtension: "json") else { return nil }
		var data: Data
		
		do {
			try data = Data(contentsOf: fileURL)
			return data
		} catch {
			// TODO: Implement error/warning
			print("Error: \(error.localizedDescription) from \(fileURL)")
			return nil
		}
	}
}
