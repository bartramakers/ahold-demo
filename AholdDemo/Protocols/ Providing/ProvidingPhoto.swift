//
//  ProvidingPhoto.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import UIKit

protocol ProvidingPhoto {
	var photo: UIImage? { get }
}
