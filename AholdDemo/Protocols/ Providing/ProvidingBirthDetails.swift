//
//  ProvidingBirthDetails.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

protocol ProvidingBirthDetails {
	var dateOfBirth: Date? { get }
	var placeOfBirth: City? { get }
}
