//
//  ProvidingContactInfo.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

protocol ProvidingContactInfo {
	var emailAddress: String? { get }
	var phoneNumber: String? { get }
	var currentPlace: City? { get }
}
