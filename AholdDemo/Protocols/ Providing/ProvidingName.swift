//
//  ProvidingName.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

protocol ProvidingName {
	var firstName: String? { get }
	var lastName: String? { get }
}

extension ProvidingName {
	var fullName: String {
		var name = ""
		if let firstName = self.firstName, firstName.isEmpty == false { name += firstName }
		if let lastName = self.lastName, lastName.isEmpty == false { name += (name.isEmpty ? "" : " ") + lastName }
		return name
	}
}
