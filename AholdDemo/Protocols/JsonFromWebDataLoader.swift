//
//  JsonFromWebDataLoader.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

protocol JsonFromWebDataLoader: JsonDataLoader {
	var url: URL { get }
}

extension JsonFromWebDataLoader {
	var resourceData: Data? {
		// TODO: Implement fetching
		return nil
	}
}
