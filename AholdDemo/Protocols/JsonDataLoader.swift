//
//  JsonDataLoader.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation

protocol JsonDataLoader {
	associatedtype ModelToLoad: Decodable
	var resourceData: Data? { get }
}

extension JsonDataLoader {
	func loadModelsFromResource() -> [ModelToLoad] {
		guard let data = self.resourceData else { return [] }
		
		let decoder = JSONDecoder()
		do {
			let models = try decoder.decode([ModelToLoad].self, from: data)
			return models
		} catch {
			// TODO: Implement proper error/warning
			print("Error: \(error.localizedDescription) from \(data)")
			return []
		}
	}
}
