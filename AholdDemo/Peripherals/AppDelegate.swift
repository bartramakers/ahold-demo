//
//  AppDelegate.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import UIKit

var app: ApplicationContext!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	var isRunningUnitTests: Bool {
		if let injectBundle = ProcessInfo.processInfo.environment["XCInjectBundle"] {
			return NSString(string: injectBundle).pathExtension == "xctest"
		}
		return false
	}
	
	func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
		loadDefaultAppContext()
		return true
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		guard app != nil else { return false }
		return app.start()
	}

	func loadDefaultAppContext() {
		guard !isRunningUnitTests else { return }

		app = ApplicationContext(applicantDataProvider: ApplicantFromJsonOnDiskDataProvider(),
								 countryDataProvider: CountryFromJsonOnDiskDataProvider())
	}
}
