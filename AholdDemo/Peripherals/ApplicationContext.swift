//
//  ApplicationContext.swift
//  AholdDemo
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import Foundation



class ApplicationContext {
	let applicants: ApplicantManager
	let countries: CountryManager

	init(applicantDataProvider: DataProvider<Applicant> = ApplicantFromJsonOnDiskDataProvider(),
		 countryDataProvider: DataProvider<Country> = CountryFromJsonOnDiskDataProvider()) {
		
		countries = CountryManager(dataProvider: countryDataProvider)
		applicants = ApplicantManager(dataProvider: applicantDataProvider)
	}
	
	@discardableResult
	func start() -> Bool {
		countries.start()
		applicants.start()
		
		return true
	}
}
