//
//  AholdDemoUITests.swift
//  AholdDemoUITests
//
//  Created by Bart Ramakers on 03/08/2018.
//  Copyright © 2018 Rawodo. All rights reserved.
//

import XCTest

class AholdDemoUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
		let testApp = XCUIApplication()
        continueAfterFailure = false
		testApp.launchArguments.append("isUITest = true")
        testApp.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testElements() {
		let scrollViewsQuery = XCUIApplication().scrollViews
		XCTAssertTrue(scrollViewsQuery.element.isHittable, "Applicant screen scrollview should be on screen")
		
		let elementsQuery = scrollViewsQuery.otherElements
		XCTAssertTrue(elementsQuery.containing(.image, identifier:"applicantPhoto").element.isHittable, "applicantPhoto should be on screen")
		XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["applicantName"]/*[[".staticTexts[\"Bart Ramakers\"]",".staticTexts[\"applicantName\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.isHittable, "Applicant name should be on screen")
		XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["applicantGender"]/*[[".staticTexts[\"Male\"]",".staticTexts[\"applicantGender\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.isHittable, "Applicant gender should be on screen")
		XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["applicantBirthDay"]/*[[".staticTexts[\"Feb 29, 1984\"]",".staticTexts[\"applicantBirthDay\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.isHittable, "Applicant birthday should be on screen")
		XCTAssertTrue(elementsQuery.staticTexts["applicantBirthPlace"].isHittable, "Applicant birthplace should be on screen")
		XCTAssertTrue(elementsQuery.staticTexts["applicantLocation"].isHittable, "Applicant location should be on screen")
		XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["applicantEmail"]/*[[".staticTexts[\"mail@bartramakers.net\"]",".staticTexts[\"applicantEmail\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.isHittable, "Applicant email should be on screen")
		XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["applicantPhone"]/*[[".staticTexts[\"+31638063401\"]",".staticTexts[\"applicantPhone\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.isHittable, "Applicant phone should be on screen")
	}
    
}
